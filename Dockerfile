FROM python:3.9
ENV PYTHONUNBUFFERED=1

RUN apt update && \
    apt install -y netcat

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . ./

EXPOSE 8000

COPY ./docker-entrypoint.sh ./wait-for /bin/
ENTRYPOINT [ "docker-entrypoint.sh" ]

# Command to run
CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000" ]
