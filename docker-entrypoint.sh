#!/bin/sh
set -e

_main() {
    echo "Updating config files..."
    wait-for database:5432 -- python manage.py migrate --noinput

    "$@"
}

_main "$@"
